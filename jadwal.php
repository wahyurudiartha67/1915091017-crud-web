<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SIM DOSEN</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="style.css">

</head>
<body>
<div class="container">
        <div class="row justify-content-center">
            <div class="col-10" style="border-style: outset;">
            <div class="header">
                <h2 style="font-weight:bold;text-shadow: 2px 2px 5px gray;">-   SISTEM INFORMASI DOSEN  -</h2><br>
            </div>
            <nav class="navbar navbar-expand-lg navbar-light bg-light";>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
              <div class="navbar-nav">

                <a class="nav-item nav-link" href="home.php">Home</a>
                <a class="nav-item nav-link" href="dosen.php">Dosen</a>
                <a class="nav-item nav-link" href="kelas.php">Kelas</a>
                <a class="nav-item nav-link" href="jadwal.php">Jadwal</a>

              </div>
            </div>
            </nav><br>
            <h3 align="center">Data Penjadwalan Dosen</h3><hr>
            <a class="btn btn-secondary" href="tambahjadwal.php" role="button">Tambah Data</a>

            <!-- Awal Card Tabel -->
	<div class="card mt-3">
	  <div class="card-header bg-secondary text-white">
	    Daftar Jadwal Dosen
	  </div>
	  <div class="card-body">
	    
	    <table class="table table-bordered table-striped">
	    	<tr>
	    		<th>No.</th>
	    		<th>ID Dosen</th>
	    		<th>ID Kelas</th>
	    		<th>Jadwal</th>
	    		<th>Mata Kuliah</th>
	    		<th>Aksi</th>
	    	</tr>
	    	
	    </table>

	  </div>
	</div>
	<!-- Akhir Card Tabel -->
            </div>
        </div>
        <div class="footer"><br>&copy; 2021. Wahyu Rudiartha. All Rights Reserved.</div>
    </div>
    
</body>
</html>