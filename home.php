<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" 
    rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" 
    crossorigin="anonymous">

    <title>SIM DOSEN</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="style.css">
  </head>
  
  <body>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-10" style="border-style: outset;">
            <div class="header">
                <h2 style="font-weight:bold;text-shadow: 2px 2px 5px gray;">-   SISTEM INFORMASI DOSEN  -</h2><br>
            </div>
            <nav class="navbar navbar-expand-lg navbar-light bg-light";>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
              <div class="navbar-nav">

                <a class="nav-item nav-link" href="home.php">Home</a>
                <a class="nav-item nav-link" href="dosen.php">Dosen</a>
                <a class="nav-item nav-link" href="kelas.php">Kelas</a>
                <a class="nav-item nav-link" href="jadwal.php">Jadwal</a>

              </div>
            </div>
            </nav>
            <br><br><br><h1 align="center">Selamat Datang</h1><br><br><br><br>
            
        </div>
        <div class="footer"><br>&copy; 2021. Wahyu Rudiartha. All Rights Reserved.</div>
    </div>
    


    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js" integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT" crossorigin="anonymous"></script>
    -->
  </body>
</html>