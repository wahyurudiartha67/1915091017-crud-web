<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SIM DOSEN</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="style.css">

</head>
<body>
<div class="container">
        <div class="row justify-content-center">
            <div class="col-10" style="border-style: outset;">
            <div class="header">
                <h2 style="font-weight:bold;text-shadow: 2px 2px 5px gray;">-   SISTEM INFORMASI DOSEN  -</h2><br>
            </div>
            <nav class="navbar navbar-expand-lg navbar-light bg-light";>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
              <div class="navbar-nav">

                <a class="nav-item nav-link" href="home.php">Home</a>
                <a class="nav-item nav-link" href="dosen.php">Dosen</a>
                <a class="nav-item nav-link" href="kelas.php">Kelas</a>
                <a class="nav-item nav-link" href="jadwal.php">Jadwal</a>

              </div>
            </div>
            </nav><br>
            <h3 align="center">Data Dosen</h3><hr>
            <a class="btn btn-secondary" href="tambahdosen.php" role="button">Tambah Data</a>

            <!-- Awal Card Tabel -->
	<div class="card mt-3">
	  <div class="card-header bg-secondary text-white">
	    Daftar Dosen
	  </div>
	  <div class="card-body">
	    
	    <table class="table table-bordered table-striped">
	    	<tr>
	    		<th>No.</th>
	    		<th>NIP</th>
	    		<th>Nama Dosen</th>
	    		<th>Prodi</th>
	    		<th>Fakultas</th>
	    		<th>Aksi</th>
	    	</tr>
	    	<?php
				include "koneksi.php";
				include "hapusdosen.php";
	    		$no = 1;
	    		$tampil = mysqli_query($koneksi, "SELECT * from dosen order by id_dosen asc");
	    		while($data = mysqli_fetch_array($tampil)) :

	    	?>
	    	<tr>
	    		<td><?=$no++;?></td>
	    		<td><?=$data['nip_dosen']?></td>
	    		<td><?=$data['nama_dosen']?></td>
	    		<td><?=$data['prodi']?></td>
	    		<td><?=$data['fakultas']?></td>
	    		<td>
	    			<a href="tambahdosen.php?hal=edit&id=<?=$data['id_dosen']?>" class="btn btn-info"> Edit </a>
	    			<a href="dosen.php?hal=hapus&id=<?=$data['id_dosen']?>" 
	    			   onclick="return confirm('Apakah yakin ingin menghapus data ini?')" class="btn btn-danger"> Hapus </a>
	    		</td>
	    	</tr>
	    <?php endwhile; //penutup perulangan while ?>
	    </table>

	  </div>
	</div>
	<!-- Akhir Card Tabel -->
              
            </div>
            
            
        </div>
        <div class="footer"><br>&copy; 2021. Wahyu Rudiartha. All Rights Reserved.</div>
    </div>
    
</body>
</html>