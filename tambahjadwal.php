<!DOCTYPE html>
<html>
<head>
	<title>SIM DOSEN</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="style.css">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

</head>
<body>
<div class="container">
        <div class="row justify-content-center">
            <div class="col-10" style="border-style: outset;">
            <div class="header">
                <h2 style="font-weight:bold;text-shadow: 2px 2px 5px gray;">-   SISTEM INFORMASI DOSEN  -</h2><br>
            </div>
            <nav class="navbar navbar-expand-lg navbar-light bg-light";>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
              <div class="navbar-nav">

                <a class="nav-item nav-link" href="home.php">Home</a>
                <a class="nav-item nav-link" href="dosen.php">Dosen</a>
                <a class="nav-item nav-link" href="kelas.php">Kelas</a>
                <a class="nav-item nav-link" href="jadwal.php">Jadwal</a>

              </div>
            </div>
            </nav><br>
            <h3 align="center">Tambah Data Dosen</h3><hr>
			
			<div class="container">
			<a class="btn btn-secondary" href="jadwal.php" role="button">Kembali</a>
			<!-- Awal Card Form -->
			<div class="card mt-3">

			<div class="card-header bg-secondary text-white">
				Form Input Data Penjadwalan
			</div>
			<div class="card-body">
				<form method="post" action="jadwal.php">
					<div class="form-group">
						<label>ID Dosen</label>
						<input type="text" name="nipdosen" value="<?=@$vnipdosen?>" class="form-control"required>
					</div>
					<div class="form-group">
						<label>ID Kelas</label>
						<input type="text" name="namadosen" value="<?=@$vnamadosen?>" class="form-control"required>
					</div>
					
					<div class="form-group">
						<label>Jadwal</label>
						<input id="datepicker"/>
                        <script>
                            $('#datepicker').datepicker({
                                uiLibrary: 'bootstrap4'
                            });
                        </script>
					</div>
                    <div class="form-group">
						<label>Mata Kuliah</label>
						<input type="text" name="prodi" value="<?=@$vprodi?>" class="form-control"required>
					</div>

					<button type="submit" class="btn btn-success" name="submit">Submit</button>
					<button type="reset" class="btn btn-danger" name="reset">Reset</button>

				</form>
			</div>
			</div>
			<!-- Akhir Card Form -->

			</div>
						
            </div>
            
        </div>
        <div class="footer"><br>&copy; 2021. Wahyu Rudiartha. All Rights Reserved.</div>
    </div>

<script type="text/javascript" src="js/bootstrap.min.js"></script>
</body>
</html>