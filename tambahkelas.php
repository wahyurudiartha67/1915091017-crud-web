<?php
	include "koneksi.php";
	//jika tombol simpan diklik
	if(isset($_POST['submit']))
	{
		//Pengujian Apakah data akan diedit atau disimpan baru
		if($_GET['hal'] == "edit")
		{
			//Data akan di edit
			$edit = mysqli_query($koneksi, "UPDATE kelas set
											 	nama_kelas = '$_POST[namakelas]',
												prodi = '$_POST[prodi]',
											 	fakultas = '$_POST[fakultas]'
											 WHERE id_kelas = '$_GET[id]'
										   ");
			if($edit) //jika edit sukses
			{
				echo "<script>
						alert('Edit data suksess!');
						document.location='kelas.php';
				     </script>";
			}
			else
			{
				echo "<script>
						alert('Edit data GAGAL!!');
						document.location='kelas.php';
				     </script>";
			}
		}
		else
		{
			//Data akan disimpan Baru
			$simpan = mysqli_query($koneksi, "INSERT INTO kelas (nama_kelas, prodi, fakultas)
										  VALUES ('$_POST[namakelas]', 
										  		 '$_POST[prodi]', 
										  		 '$_POST[fakultas]')
										 ");
			if($simpan) //jika simpan sukses
			{
				echo "<script>
						alert('Simpan data suksess!');
						document.location='kelas.php';
				     </script>";
			}
			else
			{
				echo "<script>
						alert('Simpan data GAGAL!!');
						document.location='kelas.php';
				     </script>";
			}
		}
		
	}

	//Pengujian jika tombol Edit / Hapus di klik
	if(isset($_GET['hal']))
	{
		//Pengujian jika edit Data
		if($_GET['hal'] == "edit")
		{
			//Tampilkan Data yang akan diedit
			$tampil = mysqli_query($koneksi, "SELECT * FROM kelas WHERE id_kelas = '$_GET[id]' ");
			$data = mysqli_fetch_array($tampil);
			if($data)
			{
				//Jika data ditemukan, maka data ditampung ke dalam variabel
				$vnamakelas = $data['nama_kelas'];
				$vprodi = $data['prodi'];
				$vfakultas = $data['fakultas'];
			}
		}
	}

?>

<!DOCTYPE html>
<html>
<head>
	<title>SIM DOSEN</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="style.css">
</head>
<body>
<div class="container">
        <div class="row justify-content-center">
            <div class="col-10" style="border-style: outset;">
            <div class="header">
                <h2 style="font-weight:bold;text-shadow: 2px 2px 5px gray;">-   SISTEM INFORMASI DOSEN  -</h2><br>
            </div>
            <nav class="navbar navbar-expand-lg navbar-light bg-light";>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
              <div class="navbar-nav">

                <a class="nav-item nav-link" href="home.php">Home</a>
                <a class="nav-item nav-link" href="dosen.php">Dosen</a>
                <a class="nav-item nav-link" href="kelas.php">Kelas</a>
                <a class="nav-item nav-link" href="jadwal.php">Jadwal</a>

              </div>
            </div>
            </nav><br>
            <h3 align="center">Tambah Data Kelas</h3><hr>
			
			<div class="container">
			<a class="btn btn-secondary" href="kelas.php" role="button">Kembali</a>
			<!-- Awal Card Form -->
			<div class="card mt-3">

			<div class="card-header bg-secondary text-white">
				Form Input Data Kelas
			</div>
			<div class="card-body">
				<form method="post" action="">
					<div class="form-group">
						<label>Nama Kelas</label>
						<input type="text" name="namakelas" value="<?=@$vnamakelas?>" class="form-control"required>
					</div>
					<div class="form-group">
						<label>Prodi</label>
						<input type="text" name="prodi" value="<?=@$vprodi?>" class="form-control"required>
					</div>
					<div class="form-group">
						<label>Fakultas</label>
						<input type="text" name="fakultas" value="<?=@$vfakultas?>" class="form-control"required>
					</div>

					<button type="submit" class="btn btn-success" name="submit">Submit</button>
					<button type="reset" class="btn btn-danger" name="reset">Reset</button>

				</form>
			</div>
			</div>
			<!-- Akhir Card Form -->

			</div>
						
            </div>
            
        </div>
        <div class="footer"><br>&copy; 2021. Wahyu Rudiartha. All Rights Reserved.</div>
    </div>

<script type="text/javascript" src="js/bootstrap.min.js"></script>
</body>
</html>